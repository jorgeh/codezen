import numpy as np
import matplotlib.pyplot as plt

from collections import namedtuple
from planar import Vec2 as Point

from bezier import AnchorPoint, Point, cubic_bezier, segment_from_anchors


def draw_line(p1, p2):
    """Draw a line segment connecting p1 and p2

    :p1: TODO
    :p2: TODO
    :returns: TODO

    """
    plt.plot([p1.x, p2.x], [p1.y, p2.y], 'ro')
    plt.plot([p1.x, p2.x], [p1.y, p2.y], 'y--')


def draw_cubic_segment(segment):
    """Draw a cubic bezier segment.

    Segment is a tuple (p0, p1, p2, p3).

    :segment: TODO
    :returns: TODO

    """
    p0, p1, p2, p3 = segment
    draw_line(p0, p1)
    draw_line(p2, p3)
    B = cubic_bezier(p0, p1, p2, p3)
    plt.plot([b.x for b in B], [b.y for b in B], 'b', linewidth=5.0)


def draw(segments, xlim=None, ylim=None):
    """Draw an iterable of segments.

    """
    for s in segments:
        draw_cubic_segment(s)

    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)


if __name__ == "__main__":

    segments = []

    #  segments.append((Point(0.1, 0), Point(0.3, 1),
    #                   Point(1, 2.5), Point(2, 1)))
    #  segments.append((Point(2, 1), Point(3, 3),
    #                   Point(1.8, 2.1), Point(2, 2)))
    #  segments.append((Point(2, 2), Point(2.3, 1),
    #                   Point(2.5, 5), Point(0.1, 0)))

    a0 = AnchorPoint(Point(0, 5), Point(0, 0), Point(1, 5), 'start')
    a1 = AnchorPoint(Point(1, 3), Point(1, 5), Point(1, 2), 'tangent')
    a2 = AnchorPoint(Point(0, 0), Point(1, 0), Point(1, 2), 'end')

    segments.append(segment_from_anchors(a0, a1))
    segments.append(segment_from_anchors(a1, a2))

    draw(segments, xlim=(-2, 2), ylim=(-1, 6))
