import numpy as np
from collections import namedtuple
from planar import Vec2 as Point


class AnchorPoint(object):
    """Docstring for AnchorPoint. """

    START = '__start__'
    END = '__end__'
    CORNER = '__corner__'
    TANGENT = '__tanget__'

    def __init__(self, anchor_pt,
                 prev_pt, next_pt,
                 pt_type=None):
        """TODO: to be defined1.

        :anchor: TODO
        :prev: TODO
        :next_pt: TODO
        :pt_type: 'start', 'end', 'corner' or 'tangent'

        """
        if not pt_type:
            pt_type = AnchorPoint.CORNER
        self.ptype = pt_type
        self._anchor_pt = anchor_pt
        self._prev_pt = prev_pt
        self._next_pt = next_pt

        self.prev_pt = prev_pt
        self.next_pt = next_pt

    def __repr__(self):
        return "prev: {} -> anchor: {} -> next: {}".format(self.prev_pt,
                                                           self.anchor_pt,
                                                           self.next_pt)

    @property
    def anchor_pt(self):
        return self._anchor_pt

    @anchor_pt.setter
    def anchor_pt(self, value):
        d = value - self._anchor_pt
        self._anchor_pt = value
        self._next_pt += d
        self._prev_pt += d

    @property
    def prev_pt(self):
        return self._prev_pt

    @prev_pt.setter
    def prev_pt(self, value):
        if self.ptype == AnchorPoint.START:
            self._prev_pt = self._anchor_pt
            return

        if self.ptype == AnchorPoint.TANGENT:
            m = (self._anchor_pt - self._next_pt).length
            uv = Point.polar((self._anchor_pt - value).angle)
            self._next_pt = self._anchor_pt + m * uv

        self._prev_pt = value

    @property
    def next_pt(self):
        return self._next_pt

    @next_pt.setter
    def next_pt(self, value):
        if self.ptype == AnchorPoint.END:
            self._next_pt = self._anchor_pt
            return

        if self.ptype == AnchorPoint.TANGENT:
            m = (self._anchor_pt - self._prev_pt).length
            uv = Point.polar((self._anchor_pt - value).angle)
            self._prev_pt = self._anchor_pt + m * uv

        self._next_pt = value


def cubic_bezier(p0, p1, p2, p3):
    """Compute bezier curve.

    :p0: TODO
    :p1: TODO
    :p2: TODO
    :p3: TODO
    :returns: TODO

    """
    T = np.arange(0, 1, 1e-2).tolist()
    K = [1 - t for t in T]
    B = [k**3 * p0 + 3 * k**2 * t * p1 + 3 * k * t**2 * p2 + t**3 * p3
         for t, k in zip(T, K)]
    return B


def segment_from_anchors(anchor0, anchor1):
    return (anchor0.anchor_pt, anchor0.next_pt,
            anchor1.prev_pt, anchor1.anchor_pt)
