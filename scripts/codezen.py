#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt

from bezier import AnchorPoint as AP
from bezier import Point as P
from bezier import segment_from_anchors
from bezier import cubic_bezier
from drawing import draw as draw_glyph
from shapely.geometry import LineString
from shapely.geometry import CAP_STYLE, JOIN_STYLE
from descartes import PolygonPatch

GLYPHS = {}


a0_0 = AP(P(100, 600), P(100, 600), P(120, 620), AP.START)
a0_1 = AP(P(150, 620), P(140, 620), P(200, 620), AP.TANGENT)
a0_2 = AP(P(200, 300), P(200, 320), P(200, 280), AP.TANGENT)
a0_3 = AP(P(220,   0), P(200,  20), P(220,   0), AP.END)

a1_0 = AP(P(200, 300), P(200, 300), P(180, 320), AP.START)
a1_1 = AP(P(100, 150), P(100, 300), P(100,   0), AP.TANGENT)
a1_2 = AP(P(200,   0), P(220,   0), P(240,   0), AP.TANGENT)
a1_3 = AP(P(220,  20), P(200,   0), P(220,  20), AP.END)

GLYPHS['a'] = (
    [segment_from_anchors(a0_0, a0_1),
     segment_from_anchors(a0_1, a0_2),
     segment_from_anchors(a0_2, a0_3),
     ],
    [segment_from_anchors(a1_0, a1_1),
     segment_from_anchors(a1_1, a1_2),
     segment_from_anchors(a1_2, a1_3),
     ],
)


def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    for k in GLYPHS:
        for p in GLYPHS[k]:
            plt.figure(1)
            #  draw_glyph(p, xlim=(0, 400), ylim=(-300, 900))
            b = []
            for s in p:
                b += [(v.x, v.y) for v in cubic_bezier(*s)]
            #  print b
            ls = LineString(b)
            x, y = ls.xy
            plt.plot(x, y, 'r')


            dilated = ls.buffer(50)
            eroded = dilated.buffer(-30)

            plt.figure(2)
            ax = plt.gca()
            patch2a = PolygonPatch(dilated, fc='k', ec='b', alpha=0.5, zorder=1)
            ax.add_patch(patch2a)

            polygon = eroded.__geo_interface__
            # >>> geo['type']
            # 'Polygon'
            # >>> geo['coordinates'][0][:2]
            # ((0.50502525316941682, 0.78786796564403572), (0.5247963548222736,
            # 0.8096820147509064))
            patch2b = PolygonPatch(polygon, fc='k', ec='b', alpha=0.5, zorder=2)
            ax.add_patch(patch2b)

            plt.xlim(0, 300)
            plt.ylim(-300, 900)


if __name__ == "__main__":
    main()
