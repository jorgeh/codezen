function LayerManager() {

	let _active_layer = null,
		  _layers = [],
			_update_active_callback = null;

	let me = {};

	me.add_layer = function(name, polybezier) {
		polybezier.name = name;
		_layers.push(polybezier);
	};

	me.active = function(value) {
		if (!arguments.length) return _active_layer;
		_active_layer = value;
		_update_active_callback(_active_layer);
		return me;
	};

	me.update_active_callback = function(value) {
		if (!arguments.length) return _update_active_callback;
		_update_active_callback = value;
		return me;
	};

	me.apply = function(fnc) {
		_layers.forEach(function(l) {
			fnc(l);
		});
	};

	me.layers = function() { return _layers; };
	return me;
}
