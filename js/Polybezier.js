function Polybezier(svg_element_id) {
	'use strict';

	let w = d3.select(svg_element_id).node().getBoundingClientRect().width,
  		h = d3.select(svg_element_id).node().getBoundingClientRect().height;

	let svg = d3.select(svg_element_id).append("g")
							.attr("transform", "translate(50,50)");

	let dispatch = d3.dispatch("updateCtrlPt");
	let _updateCtrlPtCbck = null;
	dispatch.on("updateCtrlPt", function(wpt) {
		if (_updateCtrlPtCbck) _updateCtrlPtCbck(wpt);
	});

	let _waypoints = [],
		  the_curve = svg.append("path").classed("bezier", true),
		  curve_thickness = 10,
		  curve_color = '#888888';

	const x = d => d.x;
	const y = d => d.y;

	let me = {name: ""};


	me.group = function() {
		return svg;
	};

	function update(shape) {

		if (arguments.length) _waypoints = shape;

		// draw lines showing "connections" between control points
		_waypoints.forEach(function(wpt, i) { draw_guides(wpt, "wpt"+i); });

		// draw control points
		draw_control_points();

		// draw the actual bezier curve
		draw_bezier();
	}

	me.waypoints = function(value) {
		if (!arguments.length) return _waypoints;
		update(value);
		return me;
	};

	/**
	 * Draw the control points (handles) of the bezier curves
	 *
	 */
	function draw_control_points() {

		let cpts = [];
		_waypoints.forEach(function(wpt, i) {
			let pts = wpt.datapoints();
			pts.forEach(function(d) { d.order = i; });
			cpts.push(...pts);
		});
		let pts = svg.selectAll("circle.ctrlpt").data(cpts);

		pts.enter()
			.append("circle")
			.attr("class", d => d.waypoint.type)
			.classed("ctrlpt", true)
			.attr("cx", x)
			.attr("cy", y)
			.call(d3.drag()
				.on("start", function(d) {
					this.__origin__ = [d.x, d.y];
				})
				.on("drag", function(d) {
					let x = Math.min(w, Math.max(0, this.__origin__[0] += d3.event.dx)),
							y = Math.min(h, Math.max(0, this.__origin__[1] += d3.event.dy));
					d.callback(Point(x, y));
					update();
					dispatch.call("updateCtrlPt", this, d.waypoint);
				})
				.on("end", function() {
					delete this.__origin__;
					update();
				}));

		pts.attr("class", d => d.waypoint.type)
			.classed("ctrlpt", true)
			.attr("cx", x)
			.attr("cy", y);

		pts.exit().remove();
	}

	function draw_bezier() {
		let num_points = _waypoints.length;
		let pts = [];
		_waypoints.forEach(function(d, i) {
			if (i >= num_points -  1) return;
			let pt0 = _waypoints[i],
				pt1 = _waypoints[i+1];
			pts.push(...cubic_bezier(pt0.loc(), pt0.to(), pt1.from(), pt1.loc()));
		});
		var lineFunction = d3.line().x(x).y(y);

		the_curve.attr("d", lineFunction(pts))
						.style("stroke", curve_color)
						.style("stroke-width", curve_thickness);
	}

	function draw_guides(waypoint, waypoint_index) {

		let pts_str = "";
		waypoint.datapoints().forEach(function(d) {
			pts_str += d.x + "," + d.y + " ";
		});
		pts_str = pts_str.substring(0, pts_str.length - 1);

		let guides = svg.selectAll("polyline.guides." + waypoint_index)
			.data([{pts: pts_str}]);

		guides.enter()
			.append("polyline")
			.classed("guides", true)
			.classed(waypoint_index, true)
			.classed(waypoint.type, true)
			.attr("points", d => d.pts);

		guides.attr("points", d => d.pts)
					.classed(waypoint.type, true);

		guides.exit().remove();
	}

	me.updateCtrlPtCbck = function(value) {
		if (!arguments.length) return _updateCtrlPtCbck;
		_updateCtrlPtCbck = value;
		return me;
	};

	me.thickness = function(value) {
		if (!arguments.length) return curve_thickness;
		curve_thickness	= value;
		draw_bezier();
		return me;
	};

	me.color = function (value) {
		if (!arguments.length) return curve_color;
		curve_color = value;
		draw_bezier();
		return me;
	};

	return me;
}


/**
 * Given 4 control points defining a cubic bezier curve, return a list of
 * "close points" on the curve.
 */
function cubic_bezier(p0, p1, p2, p3) {

	const step = 1e-2;
  let T = d3.range(0, 1+step, step),
			K = T.map(d => 1 - d),
			B = T.map(function(t) {
				let k = 1 - t;
				return {
					x: Math.pow(k, 3) * p0.x + 3 * Math.pow(k, 2) * t * p1.x +
						 3 * k * Math.pow(t, 2) * p2.x + Math.pow(t, 3) * p3.x,
					y: Math.pow(k, 3) * p0.y + 3 * Math.pow(k, 2) * t * p1.y +
						 3 * k * Math.pow(t, 2) * p2.y + Math.pow(t, 3) * p3.y
				};
			});

	return B;
}


/**
 * Hacky class to define a 2D point, defining some basic operations.
 */
let Point = function(x, y) {
	let me = {x: x, y: y};

	me.add = function(other) { return Point(me.x + other.x, me.y + other.y); };
	me.mult = function(factor) { return Point(me.x * factor, me.y * factor); };
	me.neg = function() { return Point(-me.x, -me.y); };
	me.sub = function(other) { return me.add(other.neg()); };
	me.length = function() { return Math.sqrt(Math.pow(me.x, 2) + Math.pow(me.y, 2)); };
	me.unit = function() {
		let l = me.length();
		return Point(me.x / l, me.y / l);
	};
	me.colinear = function(p0, p1) {
		let a = p0.x, b = p0.y,
			  m = p1.x, n = p1.y,
			  x = me.x, y = me.y;
		return a * (n - y) + m * (y - b) + x * (b - n) === 0;
	};
	me.toString = function() {
		let formatter = d3.format(".2f");
		// return "Point(" + formatter(me.x) + ", " + formatter(me.y) + ")";
		return "(" + formatter(me.x) + ", " + formatter(me.y) + ")";
	};
	return me;
};

Point.FROM = "__FROM__";
Point.LOC = "__LOC__";
Point.TO = "__TO__";

/**
 * Class that deals with a Bezier curve's end-point.
 *
 * An waypoint comprises the point itself (location or `loc`) and to extra
 * control points (`to`` and `from`). `to` and `from` can be constrained in
 * relationship to `loc`, depending on the `type` of waypoint. There are four
 * types of way-points:
 *
 *  - START:   The beginning of an open curve. It has no `from` control point
 *  - END:     The end of an open curve. It has no `to` control point
 *  - TANGENT: A waypoint where all three control points (`from`, `loc` and
 *             `to` lie on the line)
 *  - CORNER:  A waypoint that contains all three control points, but doesn't
 *						 inforce any constraint on them.
 *
 * Currently, if the `loc` point is moved, the other two points are also moved
 * to preserve their relative position. If one of the other points is moved the
 * second point might be moved to preserve the constraint defined by the
 * waypoint type.
 */
let WayPoint = function(name, p_from, p_loc, p_to, pt_type) {

	let me = {name: name, type : WayPoint.CORNER};

	let _from = p_from,
			_loc  = p_loc,
			_to   = p_to;

	me.setType = function(T) {
		if ([WayPoint.TANGENT, WayPoint.CORNER, WayPoint.START, WayPoint.END].includes(T)) {
			me.type = T;
		} else {
			console.warn("Unrecognized waypoint type", T, "Will use", WayPoint.CORNER);
			me.type = WayPoint.CORNER;
		}
		if (me.type != WayPoint.START && !_from) me.from(_to);
		if (me.type != WayPoint.END && !_to) me.to(_from);
	};


	if (arguments.length < 5) {
		if (_from === null) me.type = WayPoint.START;
		else if (_to === null) me.type = WayPoint.END;
		else if (_loc.colinear(_to, _from)) me.type = WayPoint.TANGENT;
	}
	else {
		me.setType(pt_type);
	}

	function attach_callbacks() {
		if (_from) {
			_from.callback = me.from;
			_from.waypoint = me;
			_from.role = Point.FROM;
		}
		if (_to) {
			_to.callback = me.to;
			_to.waypoint = me;
			_to.role = Point.TO;
		}
		_loc.callback = me.loc;
		_loc.waypoint = me;
		_loc.role = Point.LOC;
	}

	me.loc = function(value) {
		if (!arguments.length) return _loc;
		delta = value.sub(_loc);
		_loc = value;
		if (_from) _from = _from.add(delta);
		if (_to) _to = _to.add(delta);
		attach_callbacks();
		return me;
	};

	me.from = function(value) {
		if (!arguments.length) return _from;
		if (me.type === WayPoint.CORNER || me.type === WayPoint.END) _from = value;
		else if (me.type === WayPoint.START) _from = null;
		else if (me.type === WayPoint.TANGENT) {
			_from = value;
			let l = _loc.sub(_to).length();
			_to = _loc.add(_loc.sub(_from).unit().mult(l));
		}
		else {
			console.error("Unknown anchor point type " + me.type);
		}
		attach_callbacks();
		return me;
	};

	me.to = function(value) {
		if (!arguments.length) return _to;
		if (me.type === WayPoint.CORNER || me.type === WayPoint.START) _to = value;
		else if (me.type === WayPoint.END) _to = null;
		else if (me.type === WayPoint.TANGENT) {
			_to = value;
			let l = _loc.sub(_from).length();
			_from = _loc.add(_loc.sub(_to).unit().mult(l));
		}
		else {
			console.error("Unknown anchor point type " + me.type);
		}
		attach_callbacks();
		return me;
	};

	me.datapoints = function() {
		if (me.type == WayPoint.START) return [me.loc(), me.to()];
		if (me.type == WayPoint.END) return [me.from(), me.loc()];
		return [me.from(), me.loc(), me.to()];
	};

	attach_callbacks();

	return me;
};

WayPoint.TANGENT = "TANGENT";
WayPoint.CORNER  = "CORNER";
WayPoint.START   = "START";
WayPoint.END     = "END";



