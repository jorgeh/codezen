# Code Zen Font

A font designed for coding, by a coder

 - Author: Jorge Herrera
 - Date: November 2014

## Why?

Because I've had a hard time finding a font for coding that fits my taste/preferences. I must say that Adobe's *Source Code Pro* is a really nice font, which I've been using for a couple of years, but it felt a little wide for my taste. Other fonts are narrower, but not nearly as nice. So I decided to procrastinate designing my own coding font.

I hope you enjoy coding with it!

## Features

- Thin weight by default, designed with dark backgrounds in mind;
- Includes vim-powerline symbols
t Includes accentuation marks from several languages
- Included [NerdFont][] patched version, so it can work with [vim-devicons](https://github.com/ryanoasis/vim-devicons)

## Screenshots

![Normal size glyphs](screenshots/NormalSizeBlackBackground.png)
![Huge size glyphs](screenshots/HugeSizeBlackBackground.png)
![All Glyphs](screenshots/AllGlyphs.png)




#### Disclaimer

I have no previous experience designing fonts.


## License

This License Agreement is a legal Agreement between you and the publisher; please read it carefully. This Font Software was developed specially for use by software developers and includes special terms dedicated to promoting the use of the Font Software for such purposes.

**FREE PERSONAL USE**. The Font Software is free for personal use. For the purposes of this License, Personal Use is defined as any use on your own computer that involves computer programming, software development, or the composition of plain text documents in personal, professional, or non-professional contexts. It is also permitted to publish screenshots of your development environment while using the Font Software.

**SEPARATE PUBLISHING LICENSE**. If you use the Code Zen Font Software in a non-development context, or in any context where the Font Software or its appearance will be published or distributed, you will need to purchase the appropriate License. The uses noted below require a separate license:
 - Print and/or publishing of rich-text documents (such as PDF or Word)
 - Websites, with fonts served via the WebType service
 - Websites, with self-hosted fonts
 - Embedding in Applications (“Apps”)
 - Bundling of the Code Zen Font with coding or text processing software

**LIMITED MODIFICATIONS ALLOWED**. Modifications created by third party software is permitted for Personal Use, but is otherwise not permitted. Distribution of personally modified versions of the Font Software is not permitted.

**NO REDISTRIBUTION**. You may not distribute the Font Software to third parties for the purposes of permitting the display of the Font Software or allowing or facilitating redistribution of the Font Software irrespective of the terms of the redistribution. For the purposes of clarity you are not permitted to embed the Font Software or otherwise bundle the Font Software with another file. All such uses require the purchase of a special license.

**LIMITATION OF LIABILITY**. Except as may be otherwise provided for herein, the font software and related documentation is provided "AS IS" and without warranty of any kind and licensor EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS AND IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. LICENSOR DOES NOT WARRANT THAT THE OPERATION OF THE FONT SOFTWARE WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT THE FONT SOFTWARE IS WITHOUT DEFECTS. THE FONT SOFTWARE IS NOT INTENDED AND WAS NOT DESIGNED OR MANUFACTURED FOR USE IN ANY CIRCUMSTANCES WHERE THE FAILURE OF THE FONT SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY, OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE. THE FONT SOFTWARE IS NOT FAULT TOLERANT AND IS NOT INTENDED FOR USE IN THE CONTROL OR OPERATION OF PROCESS CONTROL DEVICES OR EQUIPMENT FOR MANUFACTURING. EXCEPT AS PROVIDED IN SECTION 12, UNDER NO CIRCUMSTANCES SHALL LICENSOR BE LIABLE TO LICENSEE OR ANY OTHER PARTY, WHETHER IN CONTRACT OR TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, FOR ANY SPECIAL, CONSEQUENTIAL, OR INCIDENTAL DAMAGES, INCLUDING LOST PROFITS, SAVINGS OR BUSINESS INTERRUPTION AS A RESULT OF THE USE OF THE FONT SOFTWARE EVEN IF NOTIFIED IN ADVANCE OF SUCH POSSIBILITY. Licensor's liability to licensee shall in no event exceed the cost of this font software license agreement.

**COMPLIANCE WITH LAWS**. Licensee shall be responsible for compliance with all laws, foreign and domestic, including but not limited to, all United States laws and regulations relating to the control of exports or the transfer of technology.

**ENTIRE AGREEMENT**. This Agreement constitutes the entire understanding between the parties and supersedes all previous agreements, promises, representations and negotiations between the parties concerning the Font Software.

**GOVERNING LAW AND FORUM**. The validity, construction, and performance of this agreement shall be governed by the laws of the State of New York without giving effect to its conflict of laws principles. Licensor and Licensee agree to settle all disputes, controversies, or claims relating to or arising from this Agreement in and specifically consent to personal jurisdiction of the state or federal district courts located in New York over any action arising out of or related to this Agreement. You agree that any breach of this License will cause irreparable harm to Licensor and that any damages are impossible to ascertain and that Licensor expressly shall be entitled to pursue equitable relief including, but not limited to, temporary restraining orders, and preliminary injunctions without the obligation of bond or other security in additions to other remedies under law.

**SEVERABILITY**. If any provision of this Agreement is declared by a court of competent jurisdiction to be invalid, void or unenforceable, the remaining provisions of this Agreement shall continue in full force and effect, and the invalid provision shall be replaced by Licensor with a valid and enforceable provision that most closely effects the intent of the invalid provision.

**AUTHORIZED PARTIES**. The parties, by their signatures or the downloading and installation of the Font Software represent that they are authorized to enter into this Agreement.


# Developemnt

The font was created using [FontForge][]. Just open the appropriate `.sfd` file and hack-at-it.

The fonts included in `src/glyphs` (which are only required for the [Nerd Font] compatible version) were downloaded from ['src/grlyphs'](https://github.com/ryanoasis/nerd-fonts/tree/master/src/glyphs) in the [NerdFont][] repo. The script `font-patcher` was also copied from [NerdFont][].

## Nerd Font version

For the [NerdFont][] version, you need to execute `./font-patcher CodeZen-Regular.otf --complete`. If using brewed [FontForge][], you might get the following error:

> ERROR: Nerd Fonts: [FontForge][] module is probably not installed.
> [See: http://designwithfontforge.com/en-US/Installing_Fontforge.html]**

In that case, a possible solution is outlined [here](https://github.com/ryanoasis/nerd-fonts/issues/225)


[FontForge]: https://fontforge.github.io/en-US/
[NerdFont]: https://github.com/ryanoasis/nerd-fonts
